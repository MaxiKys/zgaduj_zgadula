import random

#menuList = ['1. Start New Game', '2. Options ', '0. Exit'] - tutaj tez moze byc

def newGame():
    number = random.randint(0, 100) #number jest tylko tutaj widoczne
    print('TEST >> ', number) #pokazuje co wybral komputer
    start = True
    while start:
        print('Podaj liczbe z przedzialu 0-100.')
        your_number = int(input())
        if your_number == number:
            print('Udalo sie, brawo!')
            start = False  #bez break
        elif your_number < number:
            print ('Za malo!')
        elif your_number > number:
            print ('Za duzo!')

def mainMenu():
    menuList = ['1. Start New Game', '2. Options ', '0. Exit']
    start = True
    while start:
        for item in menuList: #item, zeby potem to wyswietlilo
            print(item) #input - pobiera wartosc, zeby jej potem uzyc
        wybor = int(input())
        if wybor == 1:
            newGame()
        elif wybor == 2:
            print('Options')
        elif wybor == 0:
            print('Wyjscie')
            start = False
        else:
            print('Podaj element z menu')

if __name__ == '__main__':
        mainMenu()
#jesli bylby tutaj newGame() - to powodowaloby bledy, mianowicie po wybraniu EXIT, gra zaczynalaby sie od nowa
#wiec skoro jest tylko mainMenu to wraca mi do menu a nie do gry